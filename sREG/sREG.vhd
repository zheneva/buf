library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sREG is
  generic (
    dimension : integer := 8
    --sr_width : integer
  );
  port (
    clk : in std_logic;
    en  : in std_logic;                                      --needed to not transfer data while 3 first clk on every new line(incoming data buferised but do not trunsferd to regisrers)
    inData : in std_logic_vector(dimension - 1 downto 0);
    Data_1 : out std_logic_vector(dimension - 1 downto 0);
    Data_2 : out std_logic_vector(dimension - 1 downto 0);
    Data_3 : out std_logic_vector(dimension - 1 downto 0);
   outData : out std_logic_vector(dimension - 1 downto 0)
  
    --Data_out                                                -- needed to pass data to next bufer(RAM) module
  ) ;
end sREG;

architecture sREG_struct of sREG is

  type reg is array (0 to 3) of std_logic_vector(7 downto 0);

  signal reg_byte : reg := (others => (others => '0'));

  -- signal reg_in : std_logic_vector(dimension - 1 downto 0);

begin

process(clk)
  begin
  if (rising_edge(clk)) then
    if (en = '1') then 

      reg_byte <= inData & reg_byte(0 to 2);

    -- reg_byte(1 to 2) <= reg_byte(0 to 1);
    -- reg_byte(0) <= inData;
      -- Data_1 <= reg_byte(0);
      -- Data_2 <= reg_byte(1);
      -- Data_3 <= reg_byte(2);
    else 
      reg_byte  <= reg_byte;
    end if;
  end if;
end process;

Data_1 <= reg_byte(0);      -- goes without delay
Data_2 <= reg_byte(1);
Data_3 <= reg_byte(2);
outData <= reg_byte(3);
end sREG_struct ; -- BUF_struct