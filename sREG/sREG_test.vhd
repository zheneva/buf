library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sREG_test is
  generic (
    dimension : integer := 8
  );
end sREG_test;
architecture sREG_test_struct of sREG_test is

component  sREG
  generic (
    dimension : integer
    --sr_width : integer
  );
  port (
    clk : in std_logic;
    en  : in std_logic; 
    inData : in std_logic_vector(dimension - 1 downto 0);
    Data_1 : out std_logic_vector(dimension - 1 downto 0);
    Data_2 : out std_logic_vector(dimension - 1 downto 0);
    Data_3 : out std_logic_vector(dimension - 1 downto 0);
   outData : out std_logic_vector(dimension - 1 downto 0)
  ) ;
end component;
  signal clk : std_logic := '0';
  signal en : std_logic := '1';
  signal inData : std_logic_vector(dimension - 1 downto 0) := (others => '0');
  signal Data_1 : std_logic_vector(dimension - 1 downto 0);
  signal Data_2 : std_logic_vector(dimension - 1 downto 0);
  signal Data_3 : std_logic_vector(dimension - 1 downto 0);
  signal outData : std_logic_vector(dimension - 1 downto 0);

begin

process 
 begin
  wait for 20 ns;
  clk <= not clk;
end process;


en <= '0' after 140 ns, '1' after 260 ns;

inData <= "00000001" after 20 ns, 
          "00000011" after 60 ns,
          "00000111" after 100 ns,
          "00001111" after 140 ns,
          "00011111" after 180 ns,
          "00111111" after 220 ns,
          "01111111" after 260 ns,
          "11111111" after 300 ns;

circ_sREG: sREG
  generic map(
    dimension => dimension
  )
  port map(
    clk => clk,
    en => en,
    inData => inData,
    Data_1 => Data_1,
    Data_2 => Data_2,
    Data_3 => Data_3,
    outData => outData
) ;


end sREG_test_struct ; -- sREG_test_struct