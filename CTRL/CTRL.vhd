entity CTRL is
    generic(
        H_b_porch : integer;
        H_s_pulse : integer;
        H_pixel   : integer
    );
    port (
        hc : in std_logic_vector(9 downto 0);
        buf_en : out std_logic;
    );
end CTRL;

architecture CTRL_struct of CTRL is

    signal ADDR : std_logic_vector(9 downto 0);

begin

ADDR <= unsigned(hc) - H_b_porch - H_s_pulse - 2 ;

buf_en <= '1' when signed(ADDR) >= 0 and signed(ADDR) < H_b_porch + H_s_pulse + H_pixel + 1  else '0';  --   ADDR >= 0 and ADDR < 785

process(ADDR)
 begin
    
end process;
end CTRL_struct;