onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /buf_test/clk
add wave -noupdate /buf_test/circ_buf/circ_ram_1/w_en
add wave -noupdate /buf_test/circ_buf/circ_ram_1/r_en
add wave -noupdate /buf_test/ADDR
add wave -noupdate /buf_test/inDATA
add wave -noupdate /buf_test/circ_buf/circ_ram_1/r_ADDR
add wave -noupdate /buf_test/circ_buf/circ_ram_1/ram
add wave -noupdate /buf_test/circ_buf/circ_ram_2/r_ADDR
add wave -noupdate /buf_test/circ_buf/circ_ram_2/ram
add wave -noupdate /buf_test/circ_buf/circ_ram_3/r_ADDR
add wave -noupdate /buf_test/circ_buf/circ_ram_3/ram
add wave -noupdate -radix decimal /buf_test/circ_buf/ramOUT_3
add wave -noupdate -radix decimal /buf_test/circ_buf/ramOUT_2
add wave -noupdate -radix decimal /buf_test/circ_buf/ramOUT_1
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {500 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {700 ns} {1700 ns}
