library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BUF is
    generic(
        b_porch : integer;
        h_pixel : integer
    );
    port (
        clk : in std_logic;
        r_en: in std_logic;
        w_en: in std_logic;
          ADDRx : in std_logic_vector(9 downto 0);   -- the real size must accomodate the size of horizontal counter
          ADDRy : in std_logic_vector(9 downto 0);   -- the real size must accomodate the size of vertical   counter
        inDATA : in std_logic_vector(7 downto 0);
        Data_1 : out std_logic_vector(7 downto 0);
        Data_2 : out std_logic_vector(7 downto 0);
        Data_3 : out std_logic_vector(7 downto 0);
        Data_4 : out std_logic_vector(7 downto 0);
        Data_5 : out std_logic_vector(7 downto 0);
        Data_6 : out std_logic_vector(7 downto 0);
        Data_7 : out std_logic_vector(7 downto 0);
        Data_8 : out std_logic_vector(7 downto 0);
        Data_9 : out std_logic_vector(7 downto 0)
    );
end BUF;

architecture BUF_strucr of BUF is


component sREG 
    generic (
        dimension : integer := 8
        --sr_width : integer
    );
    port (
        clk : in std_logic;
        en  : in std_logic;                                      --needed to not transfer data while 3 first clk on every new line(incoming data buferised but do not trunsferd to regisrers)
        inData : in std_logic_vector(dimension - 1 downto 0);
        Data_1 : out std_logic_vector(dimension - 1 downto 0);
        Data_2 : out std_logic_vector(dimension - 1 downto 0);
        Data_3 : out std_logic_vector(dimension - 1 downto 0);
        outData : out std_logic_vector(dimension - 1 downto 0)
        --Data_out                                                -- needed to pass data to next bufer(RAM) module
    ) ;
end component;

component RAM 
    generic(
            RAMsize : integer := 5
    );
    port (
        clk : in std_logic;
        r_en: in std_logic;
        w_en: in std_logic;
        r_ADDR : in std_logic_vector(9 downto 0);  -- the real size must accomodate the size of horizontal counter
        w_ADDR : in std_logic_vector(9 downto 0);
        inDATA : in std_logic_vector(7 downto 0); 
       outDATA : out std_logic_vector(7 downto 0) 
    );
end component;

    signal ramOUT_1 : std_logic_vector(7 downto 0); 
    signal ramOUT_2 : std_logic_vector(7 downto 0); 
    signal ramOUT_3 : std_logic_vector(7 downto 0); 

    signal outData_1 : std_logic_vector(7 downto 0); 
    signal outData_2 : std_logic_vector(7 downto 0); 
--    signal outData_3 : std_logic_vector(7 downto 0); 

--    signal r_ADDR   : std_logic_vector(9 downto 0);    
--    signal w_ADDR   : std_logic_vector(9 downto 0);
    signal   ADDR   : std_logic_vector(9 downto 0);
    --signal   ADDRtmp  : std_logic_vector(9 downto 0);

begin

ADDR <= std_logic_vector(unsigned(ADDRx) - (b_porch-1)) when (unsigned(ADDRx) >=  (b_porch-1) and unsigned(ADDRx) < ( (b_porch-1)+h_pixel))  else (others => '0');  --ADDR <= std_logic_vector(unsigned(ADDRx) - (b_porch-1)) 
-- ADDR <= "0000000000" after 0 ns,                                                                                      --                   when (unsigned(ADDRx) >= (b_porch-1) and unsigned(ADDRx) < ((b_porch-1)+h_pixels)) 
--         "0000000001" after 140 ns,                                                                                    --                         else (others => '0'); 
--         "0000000010" after 180 ns,
--         "0000000011" after 220 ns,
--         "0000000100" after 260 ns,
--         "0000000000" after 300 ns,
--         "0000000000" after 340 ns,
--         "0000000000" after 380 ns,
--         "0000000000" after 420 ns,
--         "0000000000" after 460 ns,
--         "0000000000" after 500 ns,
--         "0000000000" after 540 ns,
--         "0000000001" after 580 ns,
--         "0000000010" after 620 ns,
--         "0000000011" after 660 ns,
--         "0000000100" after 700 ns,
--         "0000000000" after 740 ns,
--         "0000000000" after 780 ns,
--         "0000000000" after 820 ns,
--         "0000000000" after 860 ns;

-- process(ADDRx)
--  begin 
--  ADDR <= std_logic_vector(unsigned(ADDRx) - 3);     -- ADDRx - Back porch
-- end process;                                       


-- process(ADDRx, ADDRy)
--  begin 
--  w_ADDR <= std_logic_vector(unsigned(ADDR));
--  r_ADDR <= std_logic_vector(unsigned(ADDR) - 1);   
-- end process;                                       


circ_ram_1: RAM 

    port map(
         clk => clk,
        r_en => r_en,
        w_en => w_en,
        r_ADDR => ADDR,   -- the real size must accomodate the size of horizontal counter
        w_ADDR => ADDR, 
        inDATA => inDATA,  
       outDATA => ramOUT_1 
);
circ_reg_1: sREG 

    port map (
        clk  => clk,
        en   => w_en  ,                                    --needed to not transfer data while 3 first clk on every new line(incoming data buferised but do not trunsferd to regisrers)
        inData  => ramOUT_1,
        Data_1  => Data_1,
        Data_2  => Data_2,
        Data_3  => Data_3,
        outData => outData_1
        --Data_out                                                -- needed to pass data to next bufer(RAM) module
) ;


circ_ram_2: RAM 

    port map(
         clk => clk,
        r_en => r_en,
        w_en => w_en,
        r_ADDR => ADDR,   -- the real size must accomodate the size of horizontal counter
        w_ADDR => ADDR, 
        inDATA => ramOUT_1,  
        -- inDATA => outData_1,  
       outDATA => ramOUT_2 
);
circ_reg_2: sREG 

    port map (
        clk  => clk,
        en   => w_en  ,                                    --needed to not transfer data while 3 first clk on every new line(incoming data buferised but do not trunsferd to regisrers)
        inData  => ramOUT_2,
        Data_1  => Data_4,
        Data_2  => Data_5,
        Data_3  => Data_6,
        outData => outData_2
        --Data_out                                                -- needed to pass data to next bufer(RAM) module
) ;


circ_ram_3: RAM 

    port map(
         clk => clk,
        r_en => r_en,
        w_en => w_en,
        r_ADDR => ADDR,                                   -- the real size must accomodate the size of horizontal counter
        w_ADDR => ADDR, 
        inDATA => ramOUT_2,  
        -- inDATA => outData_2,  
       outDATA => ramOUT_3 
);
circ_reg_3: sREG 

    port map (
        clk  => clk,
        en   => w_en  ,                                    --needed to not transfer data while 3 first clk on every new line(incoming data buferised but do not trunsferd to regisrers)
        inData  => ramOUT_3,
        Data_1  => Data_7,
        Data_2  => Data_8,
        Data_3  => Data_9,
        outData => open
        --Data_out                                                -- needed to pass data to next bufer(RAM) module
) ;

end architecture;