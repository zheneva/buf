library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BUF_test is
end BUF_test;

architecture BUF_test_struct of BUF_test is

component BUF 
    generic(
        b_porch : integer := 3;
        h_pixel : integer := 5
    );
    port (
           clk : in std_logic;
           r_en: in std_logic;
           w_en: in std_logic;
         ADDRx : in std_logic_vector(9 downto 0);   -- the real size must accomodate the size of horizontal counter
         ADDRy : in std_logic_vector(9 downto 0);   -- the real size must accomodate the size of vertical   counter
        inDATA : in std_logic_vector(7 downto 0);
        Data_1 : out std_logic_vector(7 downto 0);
        Data_2 : out std_logic_vector(7 downto 0);
        Data_3 : out std_logic_vector(7 downto 0);
        Data_4 : out std_logic_vector(7 downto 0);
        Data_5 : out std_logic_vector(7 downto 0);
        Data_6 : out std_logic_vector(7 downto 0);
        Data_7 : out std_logic_vector(7 downto 0);
        Data_8 : out std_logic_vector(7 downto 0);
        Data_9 : out std_logic_vector(7 downto 0)
    );
end component;

signal clk   : std_logic := '0';
signal en    : std_logic := '1';
signal r_en    : std_logic := '1';
signal w_en    : std_logic := '1';
-- signal ADDRx  : std_logic_vector(9 downto 0);
-- signal ADDRy  : std_logic_vector(9 downto 0);
signal ADDR   : std_logic_vector(9 downto 0) := (others => '0');
signal inDATA : std_logic_vector(7 downto 0);
signal Data_1 : std_logic_vector(7 downto 0);
signal Data_2 : std_logic_vector(7 downto 0);
signal Data_3 : std_logic_vector(7 downto 0);
signal Data_4 : std_logic_vector(7 downto 0);
signal Data_5 : std_logic_vector(7 downto 0);
signal Data_6 : std_logic_vector(7 downto 0);
signal Data_7 : std_logic_vector(7 downto 0);
signal Data_8 : std_logic_vector(7 downto 0);
signal Data_9 : std_logic_vector(7 downto 0);


begin

process
 begin
    wait for 20 ns;
    clk <= not clk;
end process;

process(clk)
 begin
    if rising_edge(clk) then
        if (ADDR = "0000001010" ) then
            ADDR <= (others => '0');
        else
            ADDR <= std_logic_vector(unsigned(ADDR) + 1);
        end if;
    end if;
end process;

inDATA  <=  "00000000" after 20 ns,
            "00000000" after 60 ns,
            "00000001" after 100 ns,
            "00000010" after 140 ns,
            "00000011" after 180 ns,
            "00000100" after 220 ns,
            "00000101" after 260 ns,
            "00000000" after 300 ns,
            "00000000" after 340 ns,            
            "00000000" after 380 ns,
            
            "00000000" after 420 ns,
            "00000000" after 460 ns,            
            "00000000" after 500 ns,
            "00000110" after 540 ns,
            "00000111" after 580 ns,
            "00001000" after 620 ns,
            "00001001" after 660 ns,
            "00001010" after 700 ns,
            "00000000" after 740 ns,
            "00000000" after 780 ns,
            "00000000" after 820 ns,

            "00000000" after 860 ns,
            "00000000" after 900 ns,
            "00000000" after 940 ns,
            "00001011" after 980 ns,
            "00001100" after 1020 ns,
            "00001101" after 1060 ns,
            "00001110" after 1100 ns,
            "00001111" after 1140 ns,
            "00000000" after 1180 ns,
            "00000000" after 1220 ns,
            "00000000" after 1260 ns,

            "00000000" after 1300 ns,
            "00000000" after 1340 ns,
            "00000000" after 1380 ns,
            "00010000" after 1420 ns,
            "00010001" after 1460 ns,
            "00010010" after 1500 ns,
            "00010011" after 1540 ns,
            "00010100" after 1580 ns,
            "00000000" after 1620 ns,
            "00000000" after 1660 ns,
            "00000000" after 1700 ns,

            "00000000" after 1740 ns,
            "00000000" after 1780 ns,
            "00000000" after 1820 ns,
            "00010101" after 1860 ns,
            "00010110" after 1900 ns,
            "00010111" after 1940 ns,
            "00011000" after 1980 ns,
            "00011001" after 2020 ns,
            "00000000" after 2060 ns,
            "00000000" after 2100 ns,
            "00000000" after 2140 ns,

            "00000000" after 2180 ns,
            "00000000" after 2220 ns,
            "00000000" after 2260 ns,
            "00011010" after 2300 ns,
            "00011011" after 2340 ns,
            "00011100" after 2380 ns,
            "00011101" after 2420 ns,
            "00011110" after 2460 ns,
            "00000000" after 2500 ns,
            "00000000" after 2540 ns,
            "00000000" after 2580 ns;

w_en <= '0' after 300 ns,  '1' after 540 ns,
        '0' after 740 ns,  '1' after 980 ns,
        '0' after 1180 ns, '1' after 1420 ns,  
        '0' after 1620 ns, '1' after 1860 ns,  
        '0' after 2060 ns, '1' after 2300 ns,
        '0' after 2500 ns, '1' after 2740 ns,
        '0' after 2940 ns, '1' after 3180 ns,
        '0' after 3380 ns, '1' after 3620 ns;

r_en <= '0' after 260 ns,  '1' after 500 ns,
        '0' after 700 ns,  '1' after 940 ns,
        '0' after 1140 ns, '1' after 1380 ns,  
        '0' after 1580 ns, '1' after 1820 ns,  
        '0' after 2020 ns, '1' after 2260 ns,
        '0' after 2460 ns, '1' after 2700 ns,
        '0' after 2900 ns, '1' after 3140 ns,
        '0' after 3340 ns, '1' after 3580 ns;

      
-- inDATA  <=  "00000001" after 20 ns,
--             "00000010" after 60 ns,
--             "00000011" after 100 ns,
--             "00000100" after 140 ns,
--             "00000101" after 180 ns,
--             "00000110" after 220 ns,
--             "00000111" after 260 ns,
--             "00001000" after 300 ns,
--             "00001001" after 340 ns,            
--             "00001010" after 380 ns,
--             "00001011" after 420 ns,
--             "00001100" after 460 ns,            
--             "00001101" after 500 ns,
--             "00001110" after 540 ns,
--             "00001111" after 580 ns,
--             "00010000" after 620 ns,
--             "00010001" after 660 ns,
--             "00010010" after 700 ns,
--             "00010011" after 740 ns,
--             "00010100" after 780 ns,
--             "00010101" after 820 ns,
--             "00010110" after 860 ns,
--             "00010111" after 900 ns,
--             "00011000" after 940 ns,
--             "00011001" after 960 ns,
--             "00011010" after 1000 ns,
--             "00011011" after 1040 ns,
--             "00011100" after 1080 ns,
--             "00011101" after 1120 ns,
--             "00011110" after 1260 ns,
--             "00011111" after 1300 ns,
--             "00100000" after 1320 ns;
--            "00000000" after 1300 ns;

circ_buf: BUF 
    port map(
           clk => clk,
          r_en => r_en,
          w_en => w_en,
         ADDRx => ADDR,    -- the real size must accomodate the size of horizontal counter
         ADDRy => ADDR,    -- the real size must accomodate the size of vertical   counter
        inDATA => inDATA,
        Data_1 => Data_1,
        Data_2 => Data_2,
        Data_3 => Data_3,
        Data_4 => Data_4,
        Data_5 => Data_5,
        Data_6 => Data_6,
        Data_7 => Data_7,
        Data_8 => Data_8,
        Data_9 => Data_9
);


end BUF_test_struct;