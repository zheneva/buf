library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM is
    generic(
            RAMsize : integer
    );
    port (
        clk : in std_logic;
        r_en: in std_logic;
        w_en: in std_logic;
        r_ADDR : in std_logic_vector(9 downto 0);  -- the real size must accomodate the size of horizontal counter
        w_ADDR : in std_logic_vector(9 downto 0);
        inDATA : in std_logic_vector(7 downto 0); 
       outDATA : out std_logic_vector(7 downto 0) 
    );
end RAM;
------------------------------------------------------------------
architecture RAM_struct of RAM is

    type ram_type is array(0 to RAMsize - 1) of std_logic_vector(7 downto 0);

    signal ram : ram_type := (others => (others => '0'));

begin

-- process(clk)
--  begin
--     if rising_edge(clk) then
--         if (r_en = '1') then
--             outData <= std_logic_vector(ram(TO_INTEGER(unsigned(r_ADDR))));
--         -- else
--         --     outData <= outData;
--         end if;
--     end if;
-- end process;
process(r_ADDR, r_en)
 begin
        if (r_en = '1') then
            outData <= std_logic_vector(ram(TO_INTEGER(unsigned(r_ADDR))));
        end if;
end process;



process(clk)
 begin
    if (rising_edge(clk)) then
        if (w_en = '1') then
            ram(TO_INTEGER(unsigned(r_ADDR))) <= inData;
            --outData <= std_logic_vector(ram(TO_INTEGER(unsigned(r_ADDR))));
        end if;
    end if;
end process;

end RAM_struct;