library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM_test is

end RAM_test;


architecture RAM_test_struct of RAM_test is

component RAM 
    generic(
            RAMsize : integer
    );
    port (
        clk : in std_logic;
        r_en: in std_logic;
        w_en: in std_logic;
        r_ADDR : in std_logic_vector(9 downto 0);  -- the real size must accomodate the size of horizontal counter
        w_ADDR : in std_logic_vector(9 downto 0);
        inDATA : in std_logic_vector(7 downto 0); 
       outDATA : out std_logic_vector(7 downto 0) 
    );
end component;

    signal  clk : std_logic := '0';
    signal  r_en : std_logic := '1';
    signal  w_en : std_logic := '1';
    signal  r_ADDR : std_logic_vector(9 downto 0);
    signal  w_ADDR : std_logic_vector(9 downto 0);
    signal  inDATA : std_logic_vector(7 downto 0);
    signal outDATA : std_logic_vector(7 downto 0);

begin

process
 begin
    wait for 20 ns;
    clk <= not clk;
end process;

--r_en <= '0' after 20 ns, '1' after 60 ns;
--r_en <= '1' after 180 ns, '1' after 220 ns;
-- r_en <= '1' after 20 ns;

r_ADDR <= "0000000000" after 0 ns, 
          "0000000001" after 20 ns, 
          "0000000010" after 60 ns, 
          "0000000011" after 100 ns, 
          "0000000100" after 140 ns, 
          "0000000000" after 180 ns, 
          "0000000001" after 220 ns, 
          "0000000010" after 260 ns, 
          "0000000011" after 300 ns, 
          "0000000100" after 340 ns, 
          "0000000000" after 380 ns,
          "0000000001" after 420 ns,
          "0000000010" after 460 ns,
          "0000000011" after 500 ns,
          "0000000100" after 540 ns;

-- r_ADDR <= "00000000" after 20 ns,
--           "00000001" after 60 ns,
--           "00000010" after 100 ns, 
--           "00000011" after 140 ns, 
--           "00000100" after 180 ns, 
--           "00000000" after 220 ns; 




inData <= "00000000" after 0 ns, 
          "00000001" after 20 ns,
          "00000011" after 60 ns,
          "00000111" after 100 ns,
          "00001111" after 140 ns,
          "00011111" after 180 ns,
          "00111111" after 220 ns,
          "01111111" after 260 ns,
          "11111111" after 300 ns,
          "00000000" after 340 ns,
          "00000001" after 380 ns,
          "00000011" after 420 ns,
          "00000111" after 460 ns,
          "00001111" after 500 ns,
          "00011111" after 540 ns;





circ_ram: RAM 
    generic map(
            RAMsize => 5
    )
    port map(
        clk => clk,
        r_en => r_en,
        w_en => w_en,
        r_ADDR => r_ADDR,  -- the real size must accomodate the size of horizontal counter
        w_ADDR => w_ADDR,
        inDATA => inDATA,
       outDATA => outDATA 
    );

end RAM_test_struct;